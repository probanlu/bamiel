package internal

import (
	"bamiel/internal/util"
	"fmt"
	"sync"

	"github.com/bwmarrin/discordgo"
)

// TheSession is the current session
var TheSession *discordgo.Session

// TheSessionLock is a lock for TheSession; write is considered assignment or modification of the discordgo.Session
var TheSessionLock = &sync.RWMutex{}

// OnReady initializes the bot user mention string
func OnReady(sess *discordgo.Session, event *discordgo.Ready) {
	TheSessionLock.Lock()
	TheSession = sess
	TheSessionLock.Unlock()
	userID := event.User.ID
	util.SetBotUserID(userID)
	util.IsBotUserMention[fmt.Sprintf("<@%s>", userID)] = true
	util.IsBotUserMention[fmt.Sprintf("<@!%s>", userID)] = true
	// as of writing, event.User.Mention() uses the same <@...> syntax as above, but who knows if it will change at some point
	util.IsBotUserMention[event.User.Mention()] = true
}
