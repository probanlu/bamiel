package util

import (
	"fmt"
	"strings"
	"unicode"
)

// "words" are separated by one or more space characters, except space characters can appear in words by surrounding in single or double
// quotes.

const (
	stateNormal = iota
	stateInSingleQuotes
	stateInDoubleQuotes
)

// ParseWords parses "words" from the string
func ParseWords(str string) []string {
	str = strings.TrimSpace(str)
	words := make([]string, 0, 5) // 5 is an arbitrary small starting capacity
	hasWord := false
	currentWord := &strings.Builder{}
	state := stateNormal
	isAfterBackslash := false
	for _, ch := range str {
		switch state {
		case stateNormal:
			if unicode.IsSpace(ch) {
				if hasWord {
					hasWord = false
					words = append(words, currentWord.String())
					currentWord.Reset()
				}
			} else if ch == '\'' {
				hasWord = true
				state = stateInSingleQuotes
			} else if ch == '"' {
				hasWord = true
				state = stateInDoubleQuotes
			} else {
				hasWord = true
				_, _ = currentWord.WriteRune(ch)
			}
		case stateInSingleQuotes:
			if isAfterBackslash {
				_, _ = currentWord.WriteRune(ch)
				isAfterBackslash = false
			} else if ch == '\\' {
				isAfterBackslash = true
			} else if ch == '\'' {
				state = stateNormal
			} else {
				_, _ = currentWord.WriteRune(ch)
			}
		case stateInDoubleQuotes:
			if isAfterBackslash {
				_, _ = currentWord.WriteRune(ch)
				isAfterBackslash = false
			} else if ch == '\\' {
				isAfterBackslash = true
			} else if ch == '"' {
				state = stateNormal
			} else {
				_, _ = currentWord.WriteRune(ch)
			}
		default:
			panic(fmt.Errorf("ParseWords loop got in unknown state %q", state))
		}
	}
	if hasWord {
		words = append(words, currentWord.String())
	}
	// note: for malformed str, state might not be stateNormal and isAfterBackslash might be true, but we don't really care
	return words
}
