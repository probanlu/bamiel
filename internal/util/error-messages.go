package util

import (
	"fmt"
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"
)

// SendMsgIfErr sends an error message to the channel if the error is not nil.
// It also logs with log.Printf if there was an error sending the message.
// Returns whether err was not nil.
func SendMsgIfErr(err error, sess *discordgo.Session, channelID, desc string) bool {
	if err == nil {
		return false
	}
	_, metaErr := sess.ChannelMessageSend(channelID, fmt.Sprintf("```Error %s: %s```", desc, strings.ReplaceAll(fmt.Sprintf("%v", err), "`", "(backtick)")))
	if metaErr != nil {
		log.Printf("Error sending error message in channel %s for %s: %v\n", channelID, desc, metaErr)
		log.Printf("  Error that the error message was about: %v\n", err)
	}
	return true
}

// SendMsg sends the message to the channel, logging any error with log.Printf (including the message).
func SendMsg(sess *discordgo.Session, channelID, msgStr string) (*discordgo.Message, error) {
	msg, err := sess.ChannelMessageSend(channelID, msgStr)
	if err != nil {
		log.Printf("Error sending message %q in channel %s: %v\n", msgStr, channelID, err)
	}
	return msg, err
}
