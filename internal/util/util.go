package util

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"reflect"
	"regexp"
	"strings"
	"sync"
	"unicode"

	"github.com/bwmarrin/discordgo"
)

// BotUserID gets the user ID of the bot
var BotUserID func() string

// SetBotUserID sets the user ID of the bot
var SetBotUserID func(id string)

// IsBotUserMention at str is true if str is a mention of the bot user
var IsBotUserMention = map[string]bool{}

// UserAgent is what should be added as a user-agent request header on various API requests
const UserAgent = "Bamiel v0.1.0"

var shouldPrintDebugs = false

// AsyncRequest is an HTTP request and a way to retrieve a response. nil response means error.
type AsyncRequest struct {
	Request      *http.Request
	ResponseChan chan<- *http.Response
}

func init() {
	var botUserID string
	botUserIDLock := &sync.RWMutex{}
	BotUserID = func() string {
		botUserIDLock.RLock()
		defer botUserIDLock.RUnlock()
		return botUserID
	}
	SetBotUserID = func(id string) {
		botUserIDLock.Lock()
		botUserID = id
		botUserIDLock.Unlock()
	}
}

// RemoveAllSpace returns the string with all space characters removed
func RemoveAllSpace(str string) string {
	builder := &strings.Builder{}
	for _, ch := range str {
		if !unicode.IsSpace(ch) {
			builder.WriteRune(ch)
		}
	}
	return builder.String()
}

// ToLowerAndRemoveAllSpace returns the string with all space characters removed, and all other characters in lower case
func ToLowerAndRemoveAllSpace(str string) string {
	builder := &strings.Builder{}
	for _, ch := range str {
		if !unicode.IsSpace(ch) {
			builder.WriteRune(unicode.ToLower(ch))
		}
	}
	return builder.String()
}

// LogMessageIfCommand logs the author and contents with log.Printf if the message starts with a bot mention
func LogMessageIfCommand(sess *discordgo.Session, event *discordgo.MessageCreate) {
	if event.Author.ID == BotUserID() {
		return
	}
	words := ParseWords(event.Content)
	if len(words) < 1 || !IsBotUserMention[words[0]] {
		return
	}
	message := strings.TrimSpace(event.Message.Content)
	// try to remove all known possible bot mention strings from the start
	for botMention := range IsBotUserMention {
		message = strings.TrimPrefix(message, botMention)
	}
	message = strings.TrimSpace(message)
	log.Printf(
		"Command from guild %s channel %s user %s (%q): %q\n",
		event.GuildID, event.ChannelID, event.Author.ID, fmt.Sprintf("%s#%s", event.Author.Username, event.Author.Discriminator), message)
}

// HasAdminPerms returns whether a user has admin permission for a guild (i.e. is guild owner or has a role with administrator permission) and whether there was an error in checking.
// channelID is used for sending error messages if needed.
// Additional return values are resources that had to be fetched from Discord during permission checking and may be helpful for reuse in calling function. If wasError, one or more of these may be nil.
func HasAdminPerms(sess *discordgo.Session, guildID, channelID, userID string) (hasAdminPerms bool, wasError bool, guild *discordgo.Guild, guildRolesByID map[string]*discordgo.Role, member *discordgo.Member) {
	var guildRoles []*discordgo.Role
	wasError = false
	wasErrorLock := &sync.Mutex{}
	wg := &sync.WaitGroup{}
	wg.Add(3)
	go func() {
		var err error
		guild, err = sess.Guild(guildID)
		if err != nil {
			wasErrorLock.Lock()
			if !wasError {
				wasError = true
				_ = SendMsgIfErr(err, sess, channelID, "getting guild to check permissions")
			}
			wasErrorLock.Unlock()
		}
		wg.Done()
	}()
	go func() {
		var err error
		guildRoles, err = sess.GuildRoles(guildID)
		if err != nil {
			wasErrorLock.Lock()
			if !wasError {
				wasError = true
				_ = SendMsgIfErr(err, sess, channelID, "getting guild roles to check permissions")
			}
			wasErrorLock.Unlock()
		}
		wg.Done()
	}()
	go func() {
		var err error
		member, err = sess.GuildMember(guildID, userID)
		if err != nil {
			wasErrorLock.Lock()
			if !wasError {
				wasError = true
				_ = SendMsgIfErr(err, sess, channelID, "getting member to check permissions")
			}
			wasErrorLock.Unlock()
		}
		wg.Done()
	}()
	wg.Wait()
	guildRolesByID = make(map[string]*discordgo.Role, len(guildRoles))
	for _, role := range guildRoles {
		if role != nil {
			guildRolesByID[role.ID] = role
		}
	}
	hasAdminPerms = false
	if !wasError {
		if guild.OwnerID == userID {
			hasAdminPerms = true
		} else {
			for _, id := range member.Roles {
				if guildRolesByID[id].Permissions&discordgo.PermissionAdministrator != 0 {
					hasAdminPerms = true
					break
				}
			}
		}
	}
	return hasAdminPerms, wasError, guild, guildRolesByID, member
}

var RegexpAllDigits = regexp.MustCompile(`^\d+$`)

// InitDataDirOnce checks that the data directory exists, or creates it, on the first call; successive calls do nothing
var InitDataDirOnce = func() func() {
	once := &sync.Once{}
	initDataDir := func() {
		dataDir, err := os.OpenFile("./data/", os.O_RDONLY, 0)
		if err != nil {
			if os.IsNotExist(err) {
				log.Println("Data directory does not exist; creating")
				err = os.Mkdir("./data/", 0744)
				if err != nil {
					log.Fatalf("Error creating data dir: %v\n", err)
				}
				return
			}
			log.Fatalf("Error opening data dir: %v\n", err)
		}
		dataDirInfo, err := dataDir.Stat()
		if err != nil {
			log.Fatalf("Error getting data dir info: %v\n", err)
		}
		if !dataDirInfo.IsDir() {
			log.Fatalf("Error: for some reason there's non-directory file named \"data\" in place of the data dir")
		}
		dataDir.Close()
	}
	return func() {
		once.Do(initDataDir)
	}
}()

// InitJSONData loads data from ./data/{baseName}.json and exits on error.
// data must be a pointer type.
// If the file doesn't exist, data will be initialized with emptyData() and it will be saved to a new file.
func InitJSONData(baseName string, data interface{}, emptyData func() interface{}) {
	dataReflectValue := reflect.ValueOf(data)
	if dataReflectValue.Kind() != reflect.Ptr || dataReflectValue.IsNil() {
		log.Fatalf("Error initializing %s data: data is of type %T which is not a pointer type\n", baseName, data)
	}
	InitDataDirOnce()
	dataFileName := fmt.Sprintf("./data/%s.json", baseName)
	currentDataFileContents, err := os.ReadFile(dataFileName)
	if err == nil && len(currentDataFileContents) == 0 {
		// file exists but is empty
		log.Printf("%s data file exists but is empty; saving emptyData()\n", baseName)
		dataReflectValue.Elem().Set(reflect.ValueOf(emptyData()))
		err = SaveJSONData(baseName, data)
		if err != nil {
			log.Fatalf("Error saving emptyData() to %s data file: %v\n", baseName, err)
		}
		// ignore any error for that check, as it will be caught when it happens again down below
	}
	dataFile, err := os.Open(dataFileName)
	if err != nil {
		if os.IsNotExist(err) {
			log.Printf("%s data file does not exist; creating\n", baseName)
			dataReflectValue.Elem().Set(reflect.ValueOf(emptyData()))
			err = SaveJSONData(baseName, data)
			if err != nil {
				log.Fatalf("Error creating %s data file: %v\n", baseName, err)
			}
			return
		}
		log.Fatalf("Error opening %s data file: %v\n", baseName, err)
	}
	decoder := json.NewDecoder(dataFile)
	err = decoder.Decode(data)
	if err != nil {
		log.Fatalf("Error decoding %s data file: %v\n", baseName, err)
	}
	err = dataFile.Close()
	if err != nil {
		log.Fatalf("Error closing %s data file: %v\n", baseName, err)
	}
}

// SaveJSONData saves data to ./data/{baseName}.json and restores the old file on error.
// Be careful to call while under data write lock when necessary, which is almost all the time.
// This function does several file operations, so don't call it too frequently.
func SaveJSONData(baseName string, data interface{}) error {
	// create a temporary data file, then rename the original data file,
	// then rename the temporary data file, then delete the original data file
	filenamePending := fmt.Sprintf("./data/%s-pending.json", baseName)
	dataFile, err := os.OpenFile(filenamePending, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return fmt.Errorf("creating pending %s data file: %v", baseName, err)
	}
	encoder := json.NewEncoder(dataFile)
	encoder.SetIndent("", "  ")
	encoder.SetEscapeHTML(false)
	err = encoder.Encode(data)
	if err != nil {
		_ = dataFile.Close()
		_ = os.Remove(filenamePending)
		return fmt.Errorf("encoding pending %s data file: %v", baseName, err)
	}
	err = dataFile.Close()
	if err != nil {
		_ = os.Remove(filenamePending)
		return fmt.Errorf("closing pending %s data file after creating: %v", baseName, err)
	}
	// re-read file contents to ensure all the valid JSON was written
	dataFile, err = os.OpenFile(filenamePending, os.O_RDONLY, 0)
	if err != nil {
		return fmt.Errorf("opening pending %s data file for read: %v", baseName, err)
	}
	decoder := json.NewDecoder(dataFile)
	nothing := struct{}{}
	err = decoder.Decode(&nothing)
	if err != nil {
		_ = dataFile.Close()
		_ = os.Remove(filenamePending)
		return fmt.Errorf("decoding pending %s data file: %v", baseName, err)
	}
	err = dataFile.Close()
	if err != nil {
		_ = os.Remove(filenamePending)
		return fmt.Errorf("closing pending %s data file after reading: %v", baseName, err)
	}
	// rename the original data file
	filenameActual := fmt.Sprintf("./data/%s.json", baseName)
	filenameBackup := fmt.Sprintf("./data/%s-bak.json", baseName)
	err = os.Rename(filenameActual, filenameBackup)
	if err != nil && !os.IsNotExist(err) {
		_ = os.Remove(filenamePending)
		return fmt.Errorf("renaming original %s data file: %v", baseName, err)
	}
	err = os.Rename(filenamePending, filenameActual)
	if err != nil {
		_ = os.Rename(filenameBackup, filenameActual)
		_ = os.Remove(filenamePending)
		return fmt.Errorf("renaming pending %s data file: %v", baseName, err)
	}
	// we don't really care if removing the old file fails
	_ = os.Remove(filenameBackup)
	return nil
}

// DebugPrintf calls log.Printf if shouldPrintDebugs
func DebugPrintf(format string, v ...interface{}) {
	if shouldPrintDebugs {
		log.Printf(format, v...)
	}
}

// NewDebugLogger calls log.New with os.Stdout if shouldPrintDebugs, else io.Discard
func NewDebugLogger(prefix string, flag int) *log.Logger {
	var out io.Writer
	if shouldPrintDebugs {
		out = os.Stdout
	} else {
		out = io.Discard
	}
	return log.New(out, prefix, flag)
}
