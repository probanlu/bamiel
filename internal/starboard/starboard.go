package starboard

import (
	"bamiel/internal/util"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
)

var defaultThreshold = 5
var defaultStarEmojis = func() []string {
	res := []string{"⭐", "🌟", "🎇", "✨"}
	sort.Strings(res)
	return res
}()
var maxNumStarEmojis = 10
var defaultMaxAgeNewPostSeconds = 4 * 7 * 24 * 60 * 60 // 4 weeks
var minThreshold = 3

type StarboardGuildData struct {
	IsEnabled            bool                `json:"enabled"`
	StarboardChannelID   string              `json:"channel"`
	Threshold            int                 `json:"threshold"`
	StarEmojis           []string            `json:"emojis"`
	StarEmojiSet         map[string]struct{} `json:"-"`
	MaxAgeNewPostSeconds int                 `json:"maxAgeNewPostSeconds"`
	// stored by channel to preserve starboard posts when switching away from and then back to a starboard channel
	StarboardPostsByOrigIDByChannelID map[string]map[string]*StarboardPost `json:"posts"` // this[channel][origID]
}

type StarboardPost struct {
	CreateTimeSeconds            int64                    `json:"createTimeSeconds"`
	StarboardMessageID           string                   `json:"id"` // starboard message, not original message
	OrigMessageCreateTimeSeconds int64                    `json:"origMessageCreateTimeSeconds"`
	OrigMessageChannelID         string                   `json:"origMessageChannelId"`
	OrigMessageUserID            string                   `json:"origMessageUserId"`
	OrigMessageContent           *StarboardMessageContent `json:"origMessageContent"`
	StarsSummary                 string                   `json:"starsSummary"`
}

// content is saved when starboard post is created
type StarboardMessageContent struct {
	Text        string `json:"text"`
	Attachments []*discordgo.MessageAttachment
}

var starboardGuildDatasByGuildID map[string]*StarboardGuildData
var starboardGuildDatasByGuildIDLock = &sync.RWMutex{}

func init() {
	starboardGuildDatasByGuildIDLock.Lock()
	util.InitJSONData("starboard", &starboardGuildDatasByGuildID, func() interface{} { return make(map[string]*StarboardGuildData) })
	for _, guildData := range starboardGuildDatasByGuildID {
		fillStarEmojiSet(guildData)
	}
	starboardGuildDatasByGuildIDLock.Unlock()
	minThresholdStrFromEnv := strings.TrimSpace(os.Getenv("STARBOARD_MIN_THRESHOLD"))
	minThresholdFromEnv, err := strconv.ParseInt(minThresholdStrFromEnv, 10, 32)
	if len(minThresholdStrFromEnv) != 0 && err == nil && minThresholdFromEnv >= 1 {
		minThreshold = int(minThresholdFromEnv)
	}
}

// updates any existing corresponding starboard post if star counts have changed, or creates one if threshold is reached.
// also unreacts any duplicate stars and self-stars (within the first 100 users for each emoji).
// userID is of the user reacting or unreacting
func considerForStarboard(sess *discordgo.Session, guildID, channelID, messageID string, eventEmoji *discordgo.Emoji) {
	now := time.Now()
	starboardGuildDatasByGuildIDLock.Lock()
	defer starboardGuildDatasByGuildIDLock.Unlock()
	guildData := starboardGuildDatasByGuildID[guildID]
	if guildData == nil || !guildData.IsEnabled || guildData.StarboardChannelID == "(not set)" || channelID == guildData.StarboardChannelID {
		return
	}
	if eventEmoji != nil {
		if _, ok := guildData.StarEmojiSet[eventEmoji.MessageFormat()]; !ok {
			return
		}
	}
	message, err := sess.ChannelMessage(channelID, messageID)
	if err != nil {
		log.Printf("Error getting message %s in channel %s in guild %s to check starboard reacts: %v\n", messageID, channelID, guildID, err)
		return
	}
	reacts := message.Reactions
	// count starring users and also remove duplicate stars
	starsByUserID := make(map[string]string)
	starsHistogram := make(map[string]int)
	for _, react := range reacts {
		reactMessageFormat := react.Emoji.MessageFormat()
		if _, ok := guildData.StarEmojiSet[reactMessageFormat]; !ok {
			continue
		}
		reactAPIName := react.Emoji.APIName()
		reactUsers, err := sess.MessageReactions(channelID, messageID, reactAPIName, 100, "", "")
		if err != nil {
			log.Printf("Error getting users for react %q for message %s in channel %s in guild %s to check starboard reacts: %v\n", reactMessageFormat, messageID, channelID, guildID, err)
			return
		}
		for _, user := range reactUsers {
			prevStar, hasPrevStar := starsByUserID[user.ID]
			if prevStar == reactMessageFormat {
				continue // if a user ID appears multiple times for the same react somehow, just skip the extras
			}
			if hasPrevStar || user.ID == message.Author.ID {
				err = sess.MessageReactionRemove(channelID, messageID, reactAPIName, user.ID)
				if err != nil {
					log.Printf("Error removing duplicate/self star (user %s react %q) for message %s in channel %s in guild %s: %v\n", user.ID, reactMessageFormat, messageID, channelID, guildID, err)
					// do not return, since the duplicate/self star won't be counted anyway
				}
			} else {
				starsByUserID[user.ID] = reactMessageFormat
				starsHistogram[reactMessageFormat]++
			}
		}
	}
	origMessageCreateTime, err := discordgo.SnowflakeTimestamp(messageID)
	if err != nil {
		origMessageCreateTime = time.Unix(0, 0)
	}
	starboardPostsByOrigID := guildData.StarboardPostsByOrigIDByChannelID[guildData.StarboardChannelID]
	var starboardPost *StarboardPost
	if starboardPostsByOrigID != nil {
		starboardPost = starboardPostsByOrigID[messageID]
	}
	mayNeedUpdatePost := starboardPost != nil
	needNewPost := !mayNeedUpdatePost && len(starsByUserID) >= guildData.Threshold && now.Sub(origMessageCreateTime) <= time.Duration(guildData.MaxAgeNewPostSeconds)*time.Second
	var starsSummary string
	if mayNeedUpdatePost || needNewPost {
		starsSummaryParts := make([]string, 0, len(starsHistogram))
		// enumerating reacts rather than starsHistogram makes it more likely that summary order reflects react order
		for _, react := range reacts {
			reactMessageFormat := react.Emoji.MessageFormat()
			numUsers := starsHistogram[reactMessageFormat]
			if numUsers == 0 {
				continue
			}
			starsSummaryParts = append(starsSummaryParts, fmt.Sprintf("%dx%s", numUsers, reactMessageFormat))
		}
		if len(starsSummaryParts) == 0 {
			starsSummary = "(currently no stars)"
		} else {
			starsSummary = strings.Join(starsSummaryParts, " ")
		}
	}
	needSaveGuildData := false
	if mayNeedUpdatePost && starboardPost.StarsSummary != starsSummary {
		oldStarsSummary := starboardPost.StarsSummary
		starboardPost.StarsSummary = starsSummary
		embeds := []*discordgo.MessageEmbed{makeStarboardEmbed(guildID, messageID, message.Author.AvatarURL(""), starboardPost)}
		// I do not know why discordgo.MessageEdit.Embeds is *[]*discordgo.MessageEmbed instead of []*discordgo.MessageEmbed
		_, err := sess.ChannelMessageEditComplex(&discordgo.MessageEdit{
			Embeds:          &embeds,
			AllowedMentions: &discordgo.MessageAllowedMentions{},
			ID:              starboardPost.StarboardMessageID,
			Channel:         guildData.StarboardChannelID,
		})
		if err != nil {
			log.Printf("Error editing starboard post for message %s in channel %s in guild %s: %v\n", messageID, channelID, guildID, err)
			starboardPost.StarsSummary = oldStarsSummary
			return
		}
		needSaveGuildData = true
	} else if needNewPost {
		// see if post data can be copied from a previously set starboard channel
		var pastStarboardPost *StarboardPost
		for _, pastStarboardPostsByOrigID := range guildData.StarboardPostsByOrigIDByChannelID {
			pastStarboardPost = pastStarboardPostsByOrigID[messageID]
			if pastStarboardPost != nil {
				break
			}
		}
		if pastStarboardPost == nil {
			attachments := make([]*discordgo.MessageAttachment, len(message.Attachments))
			for i, attachment := range message.Attachments {
				newAttachment := *attachment
				attachments[i] = &newAttachment
			}
			starboardPost = &StarboardPost{
				CreateTimeSeconds:            now.Unix(),
				StarboardMessageID:           "", // to be set when obtained
				OrigMessageCreateTimeSeconds: origMessageCreateTime.Unix(),
				OrigMessageChannelID:         channelID,
				OrigMessageUserID:            message.Author.ID,
				OrigMessageContent: &StarboardMessageContent{
					Text:        message.Content,
					Attachments: attachments,
				},
				StarsSummary: starsSummary,
			}
		} else {
			newStarboardPost := *pastStarboardPost
			newStarboardMessageContent := *(pastStarboardPost.OrigMessageContent)
			starboardPost = &newStarboardPost
			starboardPost.StarboardMessageID = "" // to be set when obtained
			starboardPost.OrigMessageContent = &newStarboardMessageContent
			starboardPost.OrigMessageContent.Attachments = make([]*discordgo.MessageAttachment, len(pastStarboardPost.OrigMessageContent.Attachments))
			for i, attachment := range pastStarboardPost.OrigMessageContent.Attachments {
				newAttachment := *attachment
				starboardPost.OrigMessageContent.Attachments[i] = &newAttachment
			}
			starboardPost.StarsSummary = starsSummary
		}
		embed := makeStarboardEmbed(guildID, messageID, message.Author.AvatarURL(""), starboardPost)
		starboardPostMessage, err := sess.ChannelMessageSendComplex(guildData.StarboardChannelID, &discordgo.MessageSend{
			Embeds:          []*discordgo.MessageEmbed{embed},
			AllowedMentions: &discordgo.MessageAllowedMentions{},
		})
		if err != nil {
			log.Printf("Error creating starboard post for message %s in channel %s in guild %s: %v\n", messageID, channelID, guildID, err)
			return
		}
		starboardPost.StarboardMessageID = starboardPostMessage.ID
		if starboardPostsByOrigID == nil {
			starboardPostsByOrigID = make(map[string]*StarboardPost)
			guildData.StarboardPostsByOrigIDByChannelID[guildData.StarboardChannelID] = starboardPostsByOrigID
		}
		starboardPostsByOrigID[messageID] = starboardPost
		needSaveGuildData = true
	}
	if needSaveGuildData {
		err = saveStarboardGuildDatasByGuildID()
		if err != nil {
			log.Printf("Error saving starboard data: %v\n", err)
			return
		}
	}
}

// removes any starboard posts for the given deleted message IDs (which may be of original messages and/or starboard posts in past/present starboard channel)
func considerDeletesForStarboard(sess *discordgo.Session, guildID, channelID string, messageIDs []string) {
	starboardGuildDatasByGuildIDLock.Lock()
	defer starboardGuildDatasByGuildIDLock.Unlock()
	guildData := starboardGuildDatasByGuildID[guildID]
	if guildData == nil { // guildData.IsEnabled intentionally not checked
		return
	}
	needSaveGuildData := false
	// check if each message is original message of some starboard post in any past or present starboard channel
	for pastOrPresentStarboardChannelID, starboardPostsByOrigID := range guildData.StarboardPostsByOrigIDByChannelID {
		for _, messageID := range messageIDs {
			if starboardPost, ok := starboardPostsByOrigID[messageID]; ok {
				err := sess.ChannelMessageDelete(pastOrPresentStarboardChannelID, starboardPost.StarboardMessageID)
				if err != nil {
					log.Printf("Error deleting starboard message: %v\n", err)
					// do not return
				}
				delete(starboardPostsByOrigID, messageID)
				needSaveGuildData = true
			}
		}
	}
	if starboardPostsByOrigIDForEventChannel, ok := guildData.StarboardPostsByOrigIDByChannelID[channelID]; ok {
		// messageIDs may be starboard posts in past or present starboard channel
		messageIDSet := make(map[string]struct{})
		for _, messageID := range messageIDs {
			messageIDSet[messageID] = struct{}{}
		}
		for origMessageID, starboardPost := range starboardPostsByOrigIDForEventChannel {
			if _, ok := messageIDSet[starboardPost.StarboardMessageID]; ok {
				delete(starboardPostsByOrigIDForEventChannel, origMessageID)
				needSaveGuildData = true
			}
		}
	}
	if needSaveGuildData {
		for pastOrPresentStarboardChannelID, starboardPostsByOrigID := range guildData.StarboardPostsByOrigIDByChannelID {
			if len(starboardPostsByOrigID) == 0 {
				delete(guildData.StarboardPostsByOrigIDByChannelID, pastOrPresentStarboardChannelID)
			}
		}
		err := saveStarboardGuildDatasByGuildID()
		if err != nil {
			log.Printf("Error saving starboard data: %v\n", err)
			return
		}
	}
}

func makeStarboardEmbed(guildID, origMessageID, origMessageUserAvatarURL string, starboardPost *StarboardPost) *discordgo.MessageEmbed {
	origMessageContent := starboardPost.OrigMessageContent
	var embedDesc string
	if len(origMessageContent.Text) <= 4096 {
		embedDesc = origMessageContent.Text
	} else {
		embedDesc = strings.ToValidUTF8(origMessageContent.Text[:4096-4], "") + " ..."
	}
	embed := &discordgo.MessageEmbed{
		Title:       "Starboard",
		Description: embedDesc,
		Color:       0xffac34,
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: origMessageUserAvatarURL,
		},
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:  "Info",
				Value: fmt.Sprintf("<@%s> in <#%s>\n%s\n[Link](https://discord.com/channels/%s/%s/%s)", starboardPost.OrigMessageUserID, starboardPost.OrigMessageChannelID, starboardPost.StarsSummary, guildID, starboardPost.OrigMessageChannelID, origMessageID),
			},
		},
	}
	// include the first image or video attachment in embed
	for _, attachment := range origMessageContent.Attachments {
		if strings.HasPrefix(attachment.ContentType, "image/") {
			embed.Image = &discordgo.MessageEmbedImage{URL: attachment.URL}
			break
		}
		if strings.HasPrefix(attachment.ContentType, "video/") {
			embed.Video = &discordgo.MessageEmbedVideo{URL: attachment.URL}
			break
		}
	}
	if len(origMessageContent.Attachments) > 0 {
		attachmentsStringParts := make([]string, len(origMessageContent.Attachments))
		for i, attachment := range origMessageContent.Attachments {
			attachmentsStringParts[i] = fmt.Sprintf("[(%d)](%s)", i+1, attachment.URL)
		}
		attachmentsString := strings.Join(attachmentsStringParts, " ")
		if len(attachmentsString) > 1024 {
			attachmentsStringSuffix := ""
			if len(origMessageContent.Attachments) > 1 {
				attachmentsStringSuffix = "s"
			}
			attachmentsString = fmt.Sprintf("%d attachment%s", len(origMessageContent.Attachments), attachmentsStringSuffix)
		}
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:  "Attachments",
			Value: attachmentsString,
		})
	}
	return embed
}

// OnMessageCreate handles the starboard command
func OnMessageCreate(sess *discordgo.Session, event *discordgo.MessageCreate) {
	if event.Author.ID == util.BotUserID() {
		return
	}
	words := util.ParseWords(event.Content)
	if len(words) < 1 || !util.IsBotUserMention[words[0]] {
		return
	}
	words = words[1:]
	if strings.ToLower(strings.TrimSpace(words[0])) != "starboard" {
		return
	}
	words = words[1:]
	guildID, channelID := event.GuildID, event.ChannelID
	hasAdminPerms, wasError, guild, _, _ := util.HasAdminPerms(sess, guildID, channelID, event.Author.ID)
	if wasError {
		return
	}
	if !hasAdminPerms {
		_, _ = util.SendMsg(sess, channelID, "You can't use this command (you're not the guild owner, and none of your roles have the administrator permission)")
		return
	}
	if len(words) < 1 { // should be at least <subcommand>
		sendHelp(sess, channelID)
		return
	}
	subcmd := strings.ToLower(strings.TrimSpace(words[0]))
	words = words[1:]
	var err error
	starboardGuildDatasByGuildIDLock.RLock()
	guildData := starboardGuildDatasByGuildID[guildID]
	starboardGuildDatasByGuildIDLock.RUnlock()
	if guildData == nil {
		starboardGuildDatasByGuildIDLock.Lock()
		guildData = starboardGuildDatasByGuildID[guildID]
		if guildData == nil {
			guildData = &StarboardGuildData{
				IsEnabled:                         false,
				StarboardChannelID:                "(not set)",
				Threshold:                         defaultThreshold,
				StarEmojis:                        defaultStarEmojis[:],
				StarEmojiSet:                      nil,
				MaxAgeNewPostSeconds:              defaultMaxAgeNewPostSeconds,
				StarboardPostsByOrigIDByChannelID: make(map[string]map[string]*StarboardPost),
			}
			fillStarEmojiSet(guildData)
			starboardGuildDatasByGuildID[guildID] = guildData
			err = saveStarboardGuildDatasByGuildID()
			if util.SendMsgIfErr(err, sess, channelID, "saving new default starboard data") {
				starboardGuildDatasByGuildIDLock.Unlock()
				return
			}
		}
		starboardGuildDatasByGuildIDLock.Unlock()
	}
	switch subcmd {
	case "config":
		if len(words) < 1 {
			_, _ = util.SendMsg(sess, channelID, "Missing subsubcommand")
			return
		}
		subsubcmd := strings.ToLower(strings.TrimSpace(words[0]))
		words = words[1:]
		switch subsubcmd {
		case "get":
			if len(words) > 0 {
				_, _ = util.SendMsg(sess, channelID, "No arguments can come after \"get\"")
				return
			}
			starboardGuildDatasByGuildIDLock.RLock()
			defer starboardGuildDatasByGuildIDLock.RUnlock()
			guildData = starboardGuildDatasByGuildID[guildID]
			starEmojisForEmbed := make([]string, len(guildData.StarEmojis))
			for i, emoji := range guildData.StarEmojis {
				starEmojisForEmbed[i] = fmt.Sprintf("`%s`", strings.ReplaceAll(emoji, "`", "(backtick)"))
			}
			starEmojisEmbedFieldName := fmt.Sprintf("StarEmojis : %s", strings.Join(starEmojisForEmbed, " "))
			if len(starEmojisEmbedFieldName) > 256 { // length in bytes to be conservative
				starEmojisEmbedFieldName = "StarEmojis : (too many to show)"
			}
			_, err := sess.ChannelMessageSendEmbed(channelID, &discordgo.MessageEmbed{
				Title: fmt.Sprintf("Starboard Configuration for guild %q", guild.Name),
				Fields: []*discordgo.MessageEmbedField{
					{
						Name:  fmt.Sprintf("Enabled : %v", guildData.IsEnabled),
						Value: "Whether starboard is enabled.",
					},
					{
						Name:  fmt.Sprintf("StarboardChannelID : %[1]s (<#%[1]s>)", guildData.StarboardChannelID),
						Value: "The channel that sufficiently starred messages get posted to.",
					},
					{
						Name:  fmt.Sprintf("Threshold : %d", guildData.Threshold),
						Value: fmt.Sprintf("The number of starring users that causes a message to be posted to the starboard channel (minimum %d).", minThreshold),
					},
					{
						Name:  starEmojisEmbedFieldName,
						Value: "The emojis that count as stars. Note that emojis are not validated when being set.",
					},
					{
						Name:  fmt.Sprintf("MaxAgeNewPost : %s", (time.Duration(guildData.MaxAgeNewPostSeconds) * time.Second).String()),
						Value: "The starring of messages older than this will not cause new starboard posts.\nSee https://pkg.go.dev/time#ParseDuration for syntax.",
					},
				},
			})
			if util.SendMsgIfErr(err, sess, channelID, "sending starboard configuration message") {
				return
			}
		case "set":
			if len(words) < 2 {
				_, _ = util.SendMsg(sess, channelID, "Configuration name and value must come after \"set\"")
				return
			}
			starboardGuildDatasByGuildIDLock.Lock()
			defer starboardGuildDatasByGuildIDLock.Unlock()
			guildData = starboardGuildDatasByGuildID[guildID]
			configName := strings.ToLower(strings.TrimSpace(words[0]))
			words = words[1:]
			switch configName {
			case "enabled":
				if len(words) > 1 {
					_, _ = util.SendMsg(sess, channelID, "This configuration can have only 1 value")
					return
				}
				enabledStr := strings.ToLower(strings.TrimSpace(words[0]))
				if enabledStr != "true" && enabledStr != "false" {
					_, _ = util.SendMsg(sess, channelID, "This configuration can only be true or false")
					return
				}
				guildData.IsEnabled = enabledStr == "true"
			case "starboardchannelid":
				if len(words) > 1 {
					_, _ = util.SendMsg(sess, channelID, "This configuration can have only 1 value")
					return
				}
				targetChannelID := strings.ToLower(strings.TrimSpace(words[0]))
				if !util.RegexpAllDigits.MatchString(targetChannelID) {
					_, _ = util.SendMsg(sess, channelID, "This configuration must be all-digits")
					return
				}
				targetChannel, err := sess.Channel(targetChannelID)
				if util.SendMsgIfErr(err, sess, channelID, "getting channel to verify new StarboardChannelID") {
					return
				}
				if len(targetChannel.ID) == 0 || !util.RegexpAllDigits.MatchString(targetChannel.ID) {
					_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Channel structure from Discord has unusual ID %q; aborting", targetChannel.ID))
					return
				}
				guildData.StarboardChannelID = targetChannel.ID
			case "threshold":
				if len(words) > 1 {
					_, _ = util.SendMsg(sess, channelID, "This configuration can have only 1 value")
					return
				}
				thresholdStr := strings.ToLower(strings.TrimSpace(words[0]))
				threshold, err := strconv.ParseInt(thresholdStr, 10, 32)
				if err != nil || int(threshold) < minThreshold || threshold > 100 {
					_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Threshold must be an integer between %d and 100 (inclusive)", minThreshold))
					return
				}
				guildData.Threshold = int(threshold)
			case "staremojis":
				byteCount := 0
				emojis := make([]string, 0, len(words))
				seenEmojis := make(map[string]struct{}, len(words))
				for _, word := range words {
					trimmedWord := strings.TrimSpace(word)
					if _, ok := seenEmojis[trimmedWord]; len(trimmedWord) == 0 || ok {
						continue
					}
					byteCount += len(trimmedWord)
					if byteCount > 500 || len(emojis) == maxNumStarEmojis {
						_, _ = util.SendMsg(sess, channelID, "That is too many (max 10) and/or too long (max total 500 bytes) star emojis")
						return
					}
					emojis = append(emojis, trimmedWord)
					seenEmojis[trimmedWord] = struct{}{}
				}
				sort.Strings(emojis)
				guildData.StarEmojis = emojis
				fillStarEmojiSet(guildData)
			case "maxagenewpost":
				if len(words) > 1 {
					_, _ = util.SendMsg(sess, channelID, "This configuration can have only 1 value")
					return
				}
				durationStr := strings.ToLower(strings.TrimSpace(words[0]))
				duration, err := time.ParseDuration(durationStr)
				if util.SendMsgIfErr(err, sess, channelID, "parsing duration (see https://pkg.go.dev/time#ParseDuration for syntax)") {
					return
				}
				if duration < 0 {
					_, _ = util.SendMsg(sess, channelID, "Duration must not be negative")
					return
				}
				guildData.MaxAgeNewPostSeconds = int(duration / time.Second)
			default:
				_, _ = util.SendMsg(sess, channelID, "Unrecognized configuration name")
				return
			}
			err = saveStarboardGuildDatasByGuildID()
			if util.SendMsgIfErr(err, sess, channelID, "saving starboard data") {
				return
			}
			_, _ = util.SendMsg(sess, channelID, "Configuration saved")
			return
		default:
			_, _ = util.SendMsg(sess, channelID, "Unrecognized subsubcommand")
			return
		}
	default:
		_, _ = util.SendMsg(sess, channelID, "Unrecognized subcommand")
		return
	}
}

// OnMessageReactionAdd adds/updates a starboard message as needed
func OnMessageReactionAdd(sess *discordgo.Session, event *discordgo.MessageReactionAdd) {
	guildID, channelID, messageID := event.GuildID, event.ChannelID, event.MessageID
	considerForStarboard(sess, guildID, channelID, messageID, &event.Emoji)
}

// OnMessageReactionRemove updates a starboard message as needed
func OnMessageReactionRemove(sess *discordgo.Session, event *discordgo.MessageReactionRemove) {
	guildID, channelID, messageID := event.GuildID, event.ChannelID, event.MessageID
	considerForStarboard(sess, guildID, channelID, messageID, &event.Emoji)
}

// OnMessageReactionRemoveAll updates a starboard message as needed
func OnMessageReactionRemoveAll(sess *discordgo.Session, event *discordgo.MessageReactionRemoveAll) {
	guildID, channelID, messageID := event.GuildID, event.ChannelID, event.MessageID
	considerForStarboard(sess, guildID, channelID, messageID, nil)
}

// this is where I would listen for Message Reaction Remove Emoji events - IF DISCORDGO HAD THEM

// OnMessageDelete deletes a starboard message as needed
func OnMessageDelete(sess *discordgo.Session, event *discordgo.MessageDelete) {
	guildID, channelID, messageID := event.GuildID, event.ChannelID, event.ID
	considerDeletesForStarboard(sess, guildID, channelID, []string{messageID})
}

// OnMessageDeleteBulk deletes starboard messages as needed
func OnMessageDeleteBulk(sess *discordgo.Session, event *discordgo.MessageDeleteBulk) {
	guildID, channelID := event.GuildID, event.ChannelID
	considerDeletesForStarboard(sess, guildID, channelID, event.Messages)
}

// note: something currently not handled is deletion of whole channels

func fillStarEmojiSet(guildData *StarboardGuildData) {
	if guildData.StarEmojiSet == nil {
		guildData.StarEmojiSet = make(map[string]struct{})
	} else {
		for k := range guildData.StarEmojiSet {
			delete(guildData.StarEmojiSet, k)
		}
	}
	for _, emoji := range guildData.StarEmojis {
		guildData.StarEmojiSet[emoji] = struct{}{}
	}
}

func saveStarboardGuildDatasByGuildID() error {
	return util.SaveJSONData("starboard", starboardGuildDatasByGuildID)
}

func sendHelp(sess *discordgo.Session, channelID string) {
	_, err := sess.ChannelMessageSendEmbed(channelID, messageEmbedHelpCommands)
	if err != nil {
		log.Printf("Error sending starboard commands help embed: %v\n", err)
		return
	}
}

var messageEmbedHelpCommands = &discordgo.MessageEmbed{
	Title:       "starboard",
	Description: "__Description__: View or edit configuration for a \"starboard\", where messages with at least a certain number of \"star\" reacts get posted to a channel as embeds.",
	Fields: []*discordgo.MessageEmbedField{
		{
			Name:  "starboard config get",
			Value: "__Description__: Display all current starboard config names and values.\n__Syntax__: **starboard config get**",
		},
		{
			Name:  "starboard config set",
			Value: "__Description__: Set a starboard config. For the StarEmojis config, there can be multiple values.\n__Syntax__: **starboard config set** <name> <value | value value …>",
		},
	},
}
