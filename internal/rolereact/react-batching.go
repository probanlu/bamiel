package rolereact

import (
	"bamiel/internal"
	"bamiel/internal/util"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

var reactBatchChan = make(chan reactBatchMessage, 10)

type reactBatchMessage struct {
	entry  reactBatchEntry
	action reactBatchAction
}
type reactBatchEntry struct{ guildID, channelID, messageID, userID, emoji string }
type reactBatchAction int

const (
	reactBatchActionAdd reactBatchAction = iota
	reactBatchActionDel
)

func init() {
	// set of "{guildID}-{channelID}-{messageID}-{userID}-{emoji}"
	var reactBatch map[string]struct{}
	util.InitJSONData("rolereact-batch", &reactBatch, func() interface{} { return make(map[string]struct{}) })
	// this goroutine will be the only thing that touches reactBatch, so no locks are needed
	go brokerReactBatch(reactBatch)
}

func brokerReactBatch(reactBatch map[string]struct{}) {
	tickerDurationStr := strings.TrimSpace(os.Getenv("ROLEREACT_BATCH_INTERVAL"))
	if len(tickerDurationStr) == 0 {
		tickerDurationStr = "2m"
		log.Printf("ROLEREACT_BATCH_INTERVAL environment variable missing; using %q\n", tickerDurationStr)
	}
	tickerDuration, err := time.ParseDuration(tickerDurationStr)
	if err != nil {
		log.Fatalf("Error parsing ROLEREACT_BATCH_INTERVAL (value %q): %v\n", tickerDurationStr, err)
	}
	ticker := time.NewTicker(tickerDuration)
brokerLoop:
	for {
		select {
		case msg := <-reactBatchChan:
			entryStr := msg.entry.String()
			switch msg.action {
			case reactBatchActionAdd:
				reactBatch[entryStr] = struct{}{}
			case reactBatchActionDel:
				delete(reactBatch, entryStr)
			default:
				log.Printf("Interesting: rolereact batch message for %s has unknown action %q; ignoring message", entryStr, msg.action)
				continue brokerLoop
			}
			err := saveReactBatch(reactBatch)
			if err != nil {
				log.Printf("Error saving react batch after %s %s: %v\n", msg.action, entryStr, err)
			}
		case <-ticker.C:
			runActions(reactBatch)
			// clear reactBatch regardless of any errors in runActions
			for k := range reactBatch {
				delete(reactBatch, k)
			}
			err := saveReactBatch(reactBatch)
			if err != nil {
				log.Printf("Error saving react batch after running and clearing: %v\n", err)
			}
		}
	}
}

func runActions(reactBatch map[string]struct{}) {
	var err error
	internal.TheSessionLock.RLock()
	sess := internal.TheSession
	internal.TheSessionLock.RUnlock()

	reacts := make([]*reactBatchEntry, 0, len(reactBatch))
	for str := range reactBatch {
		react := &reactBatchEntry{}
		react.SetFromString(str)
		reacts = append(reacts, react)
	}

	actionsByGuildChannelMessage := make(map[[3]string][]*RolereactAction)
	rolereactDataLock.RLock()
	for _, react := range reacts {
		guildID, channelID, messageID := react.guildID, react.channelID, react.messageID
		if _, ok := actionsByGuildChannelMessage[[3]string{guildID, channelID, messageID}]; ok {
			continue
		}
		actions := getActions(guildID, channelID, messageID)
		if len(actions) > 0 {
			actionsByGuildChannelMessage[[3]string{guildID, channelID, messageID}] = actions
		}
	}
	rolereactDataLock.RUnlock()
	if len(actionsByGuildChannelMessage) == 0 {
		return
	}

	// some parallelism can be added here later, for getting members and member roles, and separately for running actions
	membersByGuildUser := make(map[[2]string]*discordgo.Member)
	memberRolesByGuildUser := make(map[[2]string]map[string]struct{})
	emojisByMessageFormatByGuild := make(map[string]map[string]*discordgo.Emoji)
	for _, react := range reacts {
		guildID, channelID, messageID, userID, emoji := react.guildID, react.channelID, react.messageID, react.userID, react.emoji
		actions := actionsByGuildChannelMessage[[3]string{guildID, channelID, messageID}]
		if len(actions) == 0 {
			continue
		}

		// skip if none of the actions are for that emoji (to avoid unnecessary API calls for guild emojis and member)
		hasAnyMatchingAction := false
		for _, action := range actions {
			if action.Emoji == emoji {
				hasAnyMatchingAction = true
				break
			}
		}
		if !hasAnyMatchingAction {
			continue
		}

		// fetch reacting user
		member := membersByGuildUser[[2]string{guildID, userID}]
		memberRoles := memberRolesByGuildUser[[2]string{guildID, userID}]
		if member == nil {
			member, err = sess.GuildMember(guildID, userID)
			if err != nil {
				log.Printf("Error getting member for user %s in guild %s to run actions: %v\n", userID, guildID, err)
				continue
			}
			membersByGuildUser[[2]string{guildID, userID}] = member
			memberRoles = make(map[string]struct{}, len(member.Roles))
			for _, role := range member.Roles {
				memberRoles[role] = struct{}{}
			}
			memberRolesByGuildUser[[2]string{guildID, userID}] = memberRoles
		}

		// fetch guild emojis if necessary
		emojisByMessageFormat := emojisByMessageFormatByGuild[guildID]
		if emojisByMessageFormat == nil {
			areSomeCustomEmojis := false
			for _, action := range actions {
				if action.Emoji[0] == '<' {
					areSomeCustomEmojis = true
				}
			}
			if areSomeCustomEmojis {
				emojis, err := sess.GuildEmojis(guildID)
				if err != nil {
					log.Printf("Error getting emojis for guild %s to run actions with emoji API names: %v\n", guildID, err)
					continue
				}
				emojisByMessageFormat = make(map[string]*discordgo.Emoji, len(emojis))
				for _, emoji := range emojis {
					emojisByMessageFormat[emoji.MessageFormat()] = emoji
				}
				emojisByMessageFormatByGuild[guildID] = emojisByMessageFormat
			}
		}

		for i, action := range actions {
			if action.Emoji != emoji {
				continue
			}
			switch action.Verb {
			case "add":
				for _, role := range action.Roles {
					if _, ok := memberRoles[role]; ok {
						continue
					}
					err = sess.GuildMemberRoleAdd(guildID, userID, role)
					if err != nil {
						log.Printf("Error adding role %s to user %s in guild %s: %v\n", role, userID, guildID, err)
					}
					memberRoles[role] = struct{}{}
				}
			case "remove":
				for _, role := range action.Roles {
					if _, ok := memberRoles[role]; !ok {
						continue
					}
					err = sess.GuildMemberRoleRemove(guildID, userID, role)
					if err != nil {
						log.Printf("Error removing role %s from user %s in guild %s: %v\n", role, userID, guildID, err)
					}
					delete(memberRoles, role)
				}
			case "toggle":
				for _, role := range action.Roles {
					if _, ok := memberRoles[role]; ok {
						err = sess.GuildMemberRoleRemove(guildID, userID, role)
						if err != nil {
							log.Printf("Error removing role %s from user %s in guild %s: %v\n", role, userID, guildID, err)
						}
						delete(memberRoles, role)
					} else {
						err = sess.GuildMemberRoleAdd(guildID, userID, role)
						if err != nil {
							log.Printf("Error adding role %s to user %s in guild %s: %v\n", role, userID, guildID, err)
						}
						memberRoles[role] = struct{}{}
					}
				}
			default:
				log.Printf("Interesting: action %d for message %s has invalid verb %q; treating as no-op action\n", i+1, messageID, action.Verb)
			}
		}
		emojiAPIName := emoji
		if e, ok := emojisByMessageFormat[emojiAPIName]; ok {
			emojiAPIName = e.APIName()
		}
		err = sess.MessageReactionRemove(channelID, messageID, emojiAPIName, userID)
		if err != nil {
			log.Printf("Error removing user react after running actions: %v\n", err)
			continue
		}
	}
}

func (e *reactBatchEntry) String() string {
	return fmt.Sprintf("%s-%s-%s-%s-%s", e.guildID, e.channelID, e.messageID, e.userID, e.emoji)
}

func (e *reactBatchEntry) SetFromString(str string) {
	strs := strings.SplitN(str, "-", 5)
	*e = reactBatchEntry{
		guildID:   strs[0],
		channelID: strs[1],
		messageID: strs[2],
		userID:    strs[3],
		emoji:     strs[4],
	}
}

func (a reactBatchAction) String() string {
	switch a {
	case reactBatchActionAdd:
		return "adding"
	case reactBatchActionDel:
		return "deleting"
	}
	return fmt.Sprintf("{unknown reactBatchAction %d}", a)
}

func saveReactBatch(reactBatch map[string]struct{}) error {
	return util.SaveJSONData("rolereact-batch", reactBatch)
}
