package rolereact

import (
	"bamiel/internal/util"
	"fmt"
	"log"
	"strings"
	"sync"
	"unicode/utf8"

	"github.com/bwmarrin/discordgo"
)

type RolereactAction struct {
	Emoji string   `json:"emoji"` // the emoji as it appears in message content
	Verb  string   `json:"verb"`  // may be "add", "remove", or "toggle"
	Roles []string `json:"roles"` // role IDs
}

// map of guild ID to map of channel ID to map of message ID to list of actions
var rolereactData map[string]map[string]map[string][]*RolereactAction
var rolereactDataLock = &sync.RWMutex{}

func init() {
	rolereactDataLock.Lock()
	util.InitJSONData("rolereact", &rolereactData, func() interface{} { return make(map[string]map[string]map[string][]*RolereactAction) })
	rolereactDataLock.Unlock()
}

var validActionVerbs = map[string]struct{}{"add": {}, "remove": {}, "toggle": {}}

// OnMessageCreate handles the rolereact command
func OnMessageCreate(sess *discordgo.Session, event *discordgo.MessageCreate) {
	if event.Author.ID == util.BotUserID() {
		return
	}
	words := util.ParseWords(event.Content)
	if len(words) < 1 || !util.IsBotUserMention[words[0]] {
		return
	}
	words = words[1:]
	if strings.ToLower(strings.TrimSpace(words[0])) != "rolereact" {
		return
	}
	words = words[1:]
	guildID, channelID := event.GuildID, event.ChannelID
	hasAdminPerms, wasError, _, guildRolesByID, _ := util.HasAdminPerms(sess, guildID, channelID, event.Author.ID)
	if wasError {
		return
	}
	if !hasAdminPerms {
		_, _ = util.SendMsg(sess, channelID, "You can't use this command (you're not the guild owner, and none of your roles have the administrator permission)")
		return
	}
	if len(words) < 2 { // should be at least create/delete (messageId)/newmessage
		sendHelp(sess, channelID)
		return
	}
	guildRolesByFuzzyName := make(map[string][]*discordgo.Role, len(guildRolesByID))
	for _, role := range guildRolesByID {
		fuzzyName := util.ToLowerAndRemoveAllSpace(role.Name)
		if guildRolesByFuzzyName[fuzzyName] == nil {
			guildRolesByFuzzyName[fuzzyName] = []*discordgo.Role{role}
		} else {
			guildRolesByFuzzyName[fuzzyName] = append(guildRolesByFuzzyName[fuzzyName], role)
		}
	}
	subcmd := strings.ToLower(strings.TrimSpace(words[0]))
	targetMessage := strings.ToLower(strings.TrimSpace(words[1]))
	var targetMessageID string
	words = words[2:]
	var err error
	switch subcmd {
	case "create":
		var newMessage string
		shouldSendNewMessage := false
		if targetMessage == "newmessage" {
			if len(words) < 1 {
				sendHelp(sess, channelID)
				return
			}
			newMessage = strings.TrimSpace(words[len(words)-1])
			shouldSendNewMessage = true
			words = words[:len(words)-1]
		} else if !util.RegexpAllDigits.MatchString(targetMessage) {
			sendHelp(sess, channelID)
			return
		}
		rolereactDataLock.RLock()
		if !shouldSendNewMessage && getActions(guildID, channelID, targetMessage) != nil {
			rolereactDataLock.RUnlock()
			_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Message %s is already a rolereact message", targetMessage))
			return
		}
		rolereactDataLock.RUnlock()
		if len(words) < 3 || len(words)%3 != 0 {
			sendHelp(sess, channelID)
			return
		}
		// transform and validate actions
		incomingActions := parseIncomingRolereactActions(words)
		actions := make([]*RolereactAction, len(incomingActions))
		areSomeCustomEmojis := false
		incomingEmojiSet := make(map[string]struct{}, len(incomingActions))
		actionRoleIDSet := make(map[string]struct{})
		hasSentRolesNamedAsRoleIDMsg := false
		for i, action := range incomingActions {
			if utf8.RuneCountInString(action.Emoji) == 0 {
				_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Action %d has invalid emoji (empty string)", i+1))
				return
			}
			if action.Emoji[0] == '<' {
				areSomeCustomEmojis = true
			}
			if _, ok := incomingEmojiSet[action.Emoji]; ok {
				_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Action %d has duplicate emoji %q", i+1, action.Emoji))
				return
			}
			incomingEmojiSet[action.Emoji] = struct{}{}
			trimmedVerb := strings.TrimSpace(action.Verb)
			if _, ok := validActionVerbs[trimmedVerb]; !ok {
				_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Action %d has invalid verb %q", i+1, action.Verb))
				return
			}
			if len(action.Roles) == 0 {
				_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Action %d has no roles", i+1))
				return
			}
			// try to find the provided roles by ID and also by name
			actionRoleIDs := make([]string, len(action.Roles))
			for roleID := range actionRoleIDSet {
				delete(actionRoleIDSet, roleID)
			}
			for j, role := range action.Roles {
				roleWithID, hasIDMatch := guildRolesByID[role]
				rolesWithName, hasNameMatch := guildRolesByFuzzyName[util.ToLowerAndRemoveAllSpace(role)]
				var id string
				switch [2]bool{hasIDMatch, hasNameMatch} {
				case [2]bool{false, false}:
					_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Action %d has unknown role %q", i+1, role))
					return
				case [2]bool{false, true}:
					if len(rolesWithName) > 1 {
						_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Action %d has ambiguous role %q (there are multiple roles with that name)", i+1, role))
						return
					}
					id = rolesWithName[0].ID
				case [2]bool{true, false}:
					id = roleWithID.ID
				case [2]bool{true, true}:
					if len(rolesWithName) > 1 || rolesWithName[0] != roleWithID {
						var roleLitSuffix string
						if len(rolesWithName) != 1 {
							roleLitSuffix = "s"
						} else {
							roleLitSuffix = ""
						}
						if !hasSentRolesNamedAsRoleIDMsg {
							_, _ = util.SendMsg(sess, channelID, fmt.Sprintf(
								"Note: I see that role %q in action %d corresponds to a role with that ID but also %d role%s with that name. "+
									"Why are you doing this... Whatever, I'm going with the role with that ID.", role, i+1, len(rolesWithName), roleLitSuffix))
							hasSentRolesNamedAsRoleIDMsg = true
						}
					}
					id = roleWithID.ID
				}
				if _, ok := actionRoleIDSet[id]; ok {
					_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Action %d has duplicate role %q", i+1, role))
					return
				}
				actionRoleIDSet[id] = struct{}{}
				actionRoleIDs[j] = id
			}
			actions[i] = &RolereactAction{
				Emoji: action.Emoji,
				Verb:  trimmedVerb,
				Roles: actionRoleIDs,
			}
		}
		// validate custom emojis
		// possible optimization: when the message contains potential custom emojis, fetch guild emojis in parallel
		//   with sending or getting the rolereact message
		var emojisByMessageFormat map[string]*discordgo.Emoji
		if areSomeCustomEmojis {
			emojis, err := sess.GuildEmojis(guildID)
			if util.SendMsgIfErr(err, sess, channelID, "getting emojis to verify custom emojis") {
				return
			}
			emojisByMessageFormat = make(map[string]*discordgo.Emoji, len(emojis))
			for _, emoji := range emojis {
				emojisByMessageFormat[emoji.MessageFormat()] = emoji
			}
			for i, action := range actions {
				if action.Emoji[0] != '<' {
					continue
				}
				if _, ok := emojisByMessageFormat[action.Emoji]; !ok {
					_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Action %d has invalid emoji (custom emoji not found)", i+1))
					return
				}
			}
		}
		// send or retrieve the message to become the rolereact message
		if shouldSendNewMessage {
			msg, err := sess.ChannelMessageSend(channelID, newMessage)
			if util.SendMsgIfErr(err, sess, channelID, "sending rolereact message") {
				return
			}
			targetMessageID = msg.ID
		} else {
			msg, err := sess.ChannelMessage(channelID, targetMessage)
			if util.SendMsgIfErr(err, sess, channelID, "getting rolereact message") {
				return
			}
			targetMessageID = msg.ID
		}
		// if using an existing message, remove all previous reacts from that message
		if !shouldSendNewMessage {
			err = sess.MessageReactionsRemoveAll(channelID, targetMessageID)
			if util.SendMsgIfErr(err, sess, channelID, "removing previous reacts from existing message") {
				return
			}
		}
		// add base reacts
		for i, action := range actions {
			emojiAPIName := action.Emoji
			if emoji, ok := emojisByMessageFormat[emojiAPIName]; ok {
				emojiAPIName = emoji.APIName()
			}
			err = sess.MessageReactionAdd(channelID, targetMessageID, emojiAPIName)
			if util.SendMsgIfErr(err, sess, channelID, fmt.Sprintf("adding base react for action %d", i+1)) {
				if shouldSendNewMessage {
					err = sess.ChannelMessageDelete(channelID, targetMessageID)
					if err != nil {
						log.Printf("Error deleting rolereact message %s in channel %s as failure cleanup: %v\n", targetMessageID, channelID, err)
					}
				}
				return
			}
		}
		// save
		rolereactDataLock.Lock()
		setActions(guildID, channelID, targetMessageID, actions)
		err = saveRolereactData()
		rolereactDataLock.Unlock()
		if util.SendMsgIfErr(err, sess, channelID, "saving rolereact data") {
			if shouldSendNewMessage {
				err = sess.ChannelMessageDelete(channelID, targetMessageID)
				if err != nil {
					log.Printf("Error deleting rolereact message %s in channel %s as failure cleanup: %v\n", targetMessageID, channelID, err)
				}
			}
			return
		}
		// remove any reacts that people might have added in the brief time between the message being created and the actions being saved
		go func() {
			msg, err := sess.ChannelMessage(channelID, targetMessageID)
			if err != nil {
				log.Printf("Error getting target message immediately after saving rolereact to delete any stray reacts: %v\n", err)
				return
			}
			botUserID := util.BotUserID()
			for _, react := range msg.Reactions {
				if !react.Me || react.Count > 1 {
					emojiAPIName := react.Emoji.APIName()
					users, err := sess.MessageReactions(channelID, targetMessageID, emojiAPIName, 100, "", "")
					if err != nil {
						log.Printf("Error getting reacts for target message immediately after saving rolereact to delete any stray reacts: %v\n", err)
						return
					}
					for _, user := range users {
						if user.ID == botUserID {
							continue
						}
						err = sess.MessageReactionRemove(channelID, targetMessageID, emojiAPIName, user.ID)
						if err != nil {
							log.Printf("Error removing stray react from target message: %v\n", err)
							return
						}
					}
				}
			}
		}()
	case "delete":
		if len(words) > 0 {
			_, _ = util.SendMsg(sess, channelID, "When deleting, no arguments can come after the message ID")
			return
		}
		if targetMessage == "newmessage" {
			_, _ = util.SendMsg(sess, channelID, "You can't use \"newmessage\" with \"delete\", that doesn't make sense")
			return
		} else if !util.RegexpAllDigits.MatchString(targetMessage) {
			sendHelp(sess, channelID)
			return
		}
		targetMessageID = targetMessage
		rolereactDataLock.RLock()
		actions := getActions(guildID, channelID, targetMessageID)
		if actions == nil {
			rolereactDataLock.RUnlock()
			_, _ = util.SendMsg(sess, channelID, fmt.Sprintf("Message %s is not a rolereact message", targetMessageID))
			return
		}
		rolereactDataLock.RUnlock()
		rolereactDataLock.Lock()
		deleteActions(guildID, channelID, targetMessageID)
		err = saveRolereactData()
		rolereactDataLock.Unlock()
		if util.SendMsgIfErr(err, sess, channelID, "saving rolereact data") {
			return
		}
		// remove base reacts
		// possible optimization: do this in parallel, though there's really no rush
		go func() {
			areSomeCustomEmojis := false
			for _, action := range actions {
				if action.Emoji[0] == '<' {
					areSomeCustomEmojis = true
					break
				}
			}
			var emojisByMessageFormat map[string]*discordgo.Emoji
			if areSomeCustomEmojis {
				emojis, err := sess.GuildEmojis(guildID)
				if err != nil {
					log.Printf(
						"Error getting emojis to remove custom base reacts from deleted rolereact for guild %s channel %s message %s: %v\n",
						guildID, channelID, targetMessageID, err)
					return
				}
				emojisByMessageFormat = make(map[string]*discordgo.Emoji, len(emojis))
				for _, emoji := range emojis {
					emojisByMessageFormat[emoji.MessageFormat()] = emoji
				}
			}
			botUserID := util.BotUserID()
			for i, action := range actions {
				emojiAPIName := action.Emoji
				if emoji, ok := emojisByMessageFormat[emojiAPIName]; ok {
					emojiAPIName = emoji.APIName()
				} else if emojiAPIName[0] == '<' {
					// emoji must be a custom emoji that does not currently exist, so we can't look it up by message format among the
					// returned guild emojis, and I don't feel like trying to parse emoji name and ID from the message format,
					// so for now these reacts will not be removed
					continue
				}
				err = sess.MessageReactionRemove(channelID, targetMessageID, emojiAPIName, botUserID)
				if err != nil {
					log.Printf(
						"Error removing base react for action %d from deleted rolereact for guild %s channel %s message %s: %v\n",
						i+1, guildID, channelID, targetMessageID, err)
					// removing the other reacts might still succeed, and there shouldn't be that many of them, so don't return
				}
			}
		}()
	default:
		_, _ = util.SendMsg(sess, channelID, "\"create\" and \"delete\" are the only rolereact subcommands")
	}
}

// OnMessageReactionAdd performs rolereact actions
func OnMessageReactionAdd(sess *discordgo.Session, event *discordgo.MessageReactionAdd) {
	guildID, channelID, messageID, userID := event.GuildID, event.ChannelID, event.MessageID, event.UserID
	if userID == util.BotUserID() {
		return
	}
	reactBatchChan <- reactBatchMessage{reactBatchEntry{guildID, channelID, messageID, userID, event.Emoji.MessageFormat()}, reactBatchActionAdd}
}

// OnMessageReactionRemove re-adds a rolereact's base reaction(s) when required.
// It also removes users from the react batch if they unreacted before the batch was processed.
func OnMessageReactionRemove(sess *discordgo.Session, event *discordgo.MessageReactionRemove) {
	guildID, channelID, messageID, userID := event.GuildID, event.ChannelID, event.MessageID, event.UserID
	botUserID := util.BotUserID()
	if userID == botUserID {
		return
	}
	// don't even need to check actions, because we know that if it was us that added it, we want it to stay
	if event.MessageReaction.UserID == botUserID {
		// yes, the re-added react may not be in the correct position, but that's what you get for messing with it
		err := sess.MessageReactionAdd(channelID, messageID, event.Emoji.APIName())
		if err != nil {
			log.Printf(
				"Error re-adding base react on removal for guild %s channel %s message %s removing user %s emoji %s: %v\n",
				guildID, channelID, messageID, userID, event.Emoji.MessageFormat(), err)
		}
		return
	}
	reactBatchChan <- reactBatchMessage{reactBatchEntry{guildID, channelID, messageID, userID, event.Emoji.MessageFormat()}, reactBatchActionDel}
}

// OnMessageReactionRemoveAll re-adds a rolereact's base reaction(s) when required
func OnMessageReactionRemoveAll(sess *discordgo.Session, event *discordgo.MessageReactionRemoveAll) {
	guildID, channelID, messageID, userID := event.GuildID, event.ChannelID, event.MessageID, event.UserID
	if userID == util.BotUserID() {
		return
	}
	rolereactDataLock.RLock()
	actions := getActions(guildID, channelID, messageID)
	rolereactDataLock.RUnlock()
	areSomeCustomEmojis := false
	for _, action := range actions {
		if action.Emoji[0] == '<' {
			areSomeCustomEmojis = true
			break
		}
	}
	var emojisByMessageFormat map[string]*discordgo.Emoji
	if areSomeCustomEmojis {
		emojis, err := sess.GuildEmojis(guildID)
		if err != nil {
			log.Printf(
				"Error getting emojis to re-add base reacts on removal for guild %s channel %s message %s removing user %s: %v\n",
				guildID, channelID, messageID, userID, err)
			return
		}
		emojisByMessageFormat = make(map[string]*discordgo.Emoji, len(emojis))
		for _, emoji := range emojis {
			emojisByMessageFormat[emoji.MessageFormat()] = emoji
		}
	}
	for _, action := range actions {
		emojiAPIName := action.Emoji
		if emoji, ok := emojisByMessageFormat[emojiAPIName]; ok {
			emojiAPIName = emoji.APIName()
		} else if emojiAPIName[0] == '<' {
			// emoji must be a custom emoji that does not currently exist, so it can't be re-added
			continue
		}
		err := sess.MessageReactionAdd(channelID, messageID, emojiAPIName)
		if err != nil {
			log.Printf(
				"Error re-adding base react on removal for guild %s channel %s message %s removing user %s emoji %s: %v\n",
				guildID, channelID, messageID, userID, event.Emoji.MessageFormat(), err)
			return
		}
	}
}

// OnMessageDelete deletes actions for the deleted message if it's a rolereact message
func OnMessageDelete(sess *discordgo.Session, event *discordgo.MessageDelete) {
	guildID, channelID, messageID := event.GuildID, event.ChannelID, event.ID
	// would check user ID against bot user ID, but the deleting user is not passed in Message Delete events
	rolereactDataLock.RLock()
	if getActions(guildID, channelID, messageID) == nil {
		rolereactDataLock.RUnlock()
		return
	}
	rolereactDataLock.RUnlock()
	rolereactDataLock.Lock()
	deleteActions(guildID, channelID, messageID)
	err := saveRolereactData()
	rolereactDataLock.Unlock()
	if err != nil {
		log.Printf(
			"Error saving rolereact data after deleting actions for guild %s channel %s message %s since message was deleted: %v\n",
			guildID, channelID, messageID, err)
		return
	}
	log.Printf(
		"Actions deleted for guild %s channel %s message %s since message was deleted\n",
		guildID, channelID, messageID)
}

// OnMessageDeleteBulk deletes actions for any of the deleted messages that are rolereact messages
func OnMessageDeleteBulk(sess *discordgo.Session, event *discordgo.MessageDeleteBulk) {
	guildID, channelID := event.GuildID, event.ChannelID
	messageIDsWithActions := []string{}
	rolereactDataLock.RLock()
	channelActionsByMessageID := rolereactData[guildID][channelID]
	for _, messageID := range event.Messages {
		if _, ok := channelActionsByMessageID[messageID]; ok {
			messageIDsWithActions = append(messageIDsWithActions, messageID)
		}
	}
	rolereactDataLock.RUnlock()
	if len(messageIDsWithActions) == 0 {
		return
	}
	rolereactDataLock.Lock()
	for _, messageID := range messageIDsWithActions {
		deleteActions(guildID, channelID, messageID)
	}
	err := saveRolereactData()
	rolereactDataLock.Unlock()
	messageIDsJoined := strings.Join(messageIDsWithActions, ",")
	if err != nil {
		log.Printf(
			"Error saving rolereact data after deleting actions for guild %s channel %s message(s) %s since the messages were deleted as part of a bulk delete: %v\n",
			guildID, channelID, messageIDsJoined, err)
		return
	}
	log.Printf(
		"Actions deleted for guild %s channel %s message(s) %s since the messages were deleted as part of a bulk delete\n",
		guildID, channelID, messageIDsJoined)
}

// note: assumes 3 divides length of words
// note: repurposes the rolereactAction model to store incoming strings instead of IDs
func parseIncomingRolereactActions(words []string) []*RolereactAction {
	actions := make([]*RolereactAction, len(words)/3)
	for i := range actions {
		roles := strings.Split(words[i*3+2], ",")
		nonEmptyRoles := make([]string, 0, len(roles))
		for _, role := range roles {
			role = strings.TrimSpace(role)
			if len(role) > 0 {
				nonEmptyRoles = append(nonEmptyRoles, role)
			}
		}
		actions[i] = &RolereactAction{
			Emoji: strings.TrimSpace(words[i*3+0]),
			Verb:  strings.ToLower(strings.TrimSpace(words[i*3+1])),
			Roles: nonEmptyRoles,
		}
	}
	return actions
}

func sendHelp(sess *discordgo.Session, channelID string) {
	_, err := sess.ChannelMessageSendEmbed(channelID, messageEmbedHelpCommands)
	if err != nil {
		log.Printf("Error sending rolereact commands help embed: %v\n", err)
		return
	}
	_, err = sess.ChannelMessageSendEmbed(channelID, messageEmbedHelpCommandCreate)
	if err != nil {
		log.Printf("Error sending rolereact create command help embed: %v\n", err)
		return
	}
	_, err = sess.ChannelMessageSendEmbed(channelID, messageEmbedHelpCommandDelete)
	if err != nil {
		log.Printf("Error sending rolereact delete command help embed: %v\n", err)
		return
	}
	_, err = sess.ChannelMessageSendEmbed(channelID, messageEmbedHelpActions)
	if err != nil {
		log.Printf("Error sending rolereact actions help embed: %v\n", err)
		return
	}
}

func saveRolereactData() error {
	return util.SaveJSONData("rolereact", rolereactData)
}

// note: only call while under at least data read lock
func getActions(guildID, channelID, messageID string) []*RolereactAction {
	if rolereactData[guildID] == nil || rolereactData[guildID][channelID] == nil {
		return nil
	}
	return rolereactData[guildID][channelID][messageID]
}

// note: only call while under data write lock
func setActions(guildID, channelID, messageID string, actions []*RolereactAction) {
	if rolereactData[guildID] == nil {
		rolereactData[guildID] = map[string]map[string][]*RolereactAction{}
	}
	if rolereactData[guildID][channelID] == nil {
		rolereactData[guildID][channelID] = map[string][]*RolereactAction{}
	}
	rolereactData[guildID][channelID][messageID] = actions
}

// note: only call while under data write lock
func deleteActions(guildID, channelID, messageID string) {
	delete(rolereactData[guildID][channelID], messageID)
	if len(rolereactData[guildID][channelID]) == 0 {
		delete(rolereactData[guildID], channelID)
		if len(rolereactData[guildID]) == 0 {
			delete(rolereactData, guildID)
		}
	}
}

var messageEmbedHelpCommands = &discordgo.MessageEmbed{
	Title:       "rolereact",
	Description: "__Description__: Designate an existing message as a, or create a new, \"rolereact\" message which will manipulate a user's roles when they react to it with different emojis.\n__Syntax__: **rolereact** <**create** | **delete**> <arguments>\n\n__Subcommands__:\n\u2003- **create**\n\u2003- **delete**",
}

var messageEmbedHelpCommandCreate = &discordgo.MessageEmbed{
	Fields: []*discordgo.MessageEmbedField{
		{
			Name:  "rolereact create <messageID>",
			Value: "__Description__: Create a rolereact on an existing message in the same channel as the command. See further below for a description of actions.\n__Syntax__: **rolereact create** <messageID> <action | action action …>\n__Example__:\n```rolereact create 757444257962524683\n\n😞\ntoggle\n'beans off toast',gorgonzola\n\n🙃\ntoggle\npastrami```",
		},
		{
			Name:  "rolereact create newmessage",
			Value: "__Description__: Create a rolereact with a new message in the same channel as the command. See further below for a description of actions.\n__Syntax__: **rolereact create newmessage** <action | action action …> <message>\n__Example__:\n```rolereact create newmessage\n\n😮\nadd\n'pineapple tree?',504710675772473374,sweet\n\n👍\ntoggle\nmetametamoderator\n\n\"this is a message\"```",
		},
	},
}

var messageEmbedHelpCommandDelete = &discordgo.MessageEmbed{
	Fields: []*discordgo.MessageEmbedField{
		{
			Name:  "rolereact delete",
			Value: "__Description__: Delete a rolereact from a message in the same channel as the command.\n__Syntax__: **rolereact delete** <messageID>\n__Example__:\n```rolereact delete 757444257962524683```",
		},
	},
}

var messageEmbedHelpActions = &discordgo.MessageEmbed{
	Title:       "__**Actions**__",
	Description: "Actions are stackable statements that are passed as arguments to a rolereact subcommand.\nEach action is composed of an **Emoji**, a **Verb**, and a **Roles**.\nActions are separated by whitespace (spaces, tabs, or newlines), but also the components within each action are separated by whitespace.\n\n__Syntax__: <**Emoji**> <**Verb**> <**Roles**>\n\n__Example__:\n```😤\nremove\n'CLOWN PERSON',bubbles```",
	Fields: []*discordgo.MessageEmbedField{
		{
			Name:  "***Emoji:***",
			Value: "<any emoji available in the guild>",
		},
		{
			Name:  "***Verb:***",
			Value: "__Description__: A verb indicates what will be done with the action's roles when a user reacts.\n__Syntax__: **add** | **remove** | **toggle**",
		},
		{
			Name:  "***Roles:***",
			Value: "__Description__: A valid role is any role in the current guild. Multiple roles are separated by commas, with no whitespace around the commas.\n__Syntax__: <role> | <role>,<role>,...\n__Formats__:\n\u2003- Role ID\n\u2003- Role name\n\u2003\u2003- Use single or double quotes for role names with spaces",
		},
	},
}
