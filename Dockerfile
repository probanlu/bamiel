FROM golang:1.20.1-alpine3.17 AS build
WORKDIR /app
ENV CGO_ENABLED 0
COPY . .
RUN go build -o bamiel cmd/bamiel/main.go

FROM busybox
WORKDIR /app
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=build /etc/ssl/cert.pem /etc/ssl/cert.pem
COPY --from=build /app/bamiel /app/bamiel

# note: -S means system group/user
RUN addgroup -S bamielg && adduser -S -G bamielg bamiel
RUN chown bamiel:bamielg /app
USER bamiel

CMD ["/app/bamiel"]
