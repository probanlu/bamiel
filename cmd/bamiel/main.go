package main

import (
	"log"
	"os"
	"strings"

	"bamiel/internal"
	"bamiel/internal/rolereact"
	"bamiel/internal/starboard"
	"bamiel/internal/util"

	"github.com/bwmarrin/discordgo"
)

func main() {
	log.Println("Bamiel main start")
	botToken := strings.TrimSpace(os.Getenv("BOT_TOKEN"))
	if len(botToken) == 0 {
		log.Fatalln("BOT_TOKEN environment variable is required")
	}
	sess, err := discordgo.New("Bot " + botToken)
	if err != nil {
		log.Fatalf("Error creating DiscordGo session: %v\n", err)
	}
	logLevelStr := strings.TrimSpace(os.Getenv("DISCORDGO_LOG_LEVEL"))
	var logLevel int
	switch strings.ToLower(logLevelStr) {
	case "error":
		logLevel = discordgo.LogError
	case "warning":
		logLevel = discordgo.LogWarning
	case "informational":
		logLevel = discordgo.LogInformational
	case "debug":
		logLevel = discordgo.LogDebug
	default:
		log.Printf("DISCORDGO_LOG_LEVEL environment variable missing or unrecognized (value %q); using %q\n", logLevelStr, "warning")
		logLevel = discordgo.LogWarning
	}
	sess.LogLevel = logLevel
	sess.StateEnabled = false // we don't have broad enough intents for "state" to be useful
	_ = sess.AddHandler(internal.OnReady)
	_ = sess.AddHandler(util.LogMessageIfCommand)
	_ = sess.AddHandler(rolereact.OnMessageCreate)
	_ = sess.AddHandler(rolereact.OnMessageReactionAdd)
	_ = sess.AddHandler(rolereact.OnMessageReactionRemove)
	_ = sess.AddHandler(rolereact.OnMessageReactionRemoveAll)
	_ = sess.AddHandler(rolereact.OnMessageDelete)
	_ = sess.AddHandler(rolereact.OnMessageDeleteBulk)
	_ = sess.AddHandler(starboard.OnMessageCreate)
	_ = sess.AddHandler(starboard.OnMessageReactionAdd)
	_ = sess.AddHandler(starboard.OnMessageReactionRemove)
	_ = sess.AddHandler(starboard.OnMessageReactionRemoveAll)
	_ = sess.AddHandler(starboard.OnMessageDelete)
	_ = sess.AddHandler(starboard.OnMessageDeleteBulk)
	sess.Identify.Intents = discordgo.MakeIntent(
		discordgo.IntentsGuildMessages |
			discordgo.IntentsGuildMessageReactions |
			discordgo.IntentMessageContent)
	err = sess.Open()
	if err != nil {
		log.Fatalf("Error opening WebSocket connection to Discord: %v\n", err)
	}
	log.Println("Opened WebSocket connection to Discord")
	//
	// http.HandleFunc("/", func(response http.ResponseWriter, request *http.Request) {
	// 	response.Write([]byte("hi"))
	// })
	// err = http.ListenAndServe(":4938", nil)
	// if err != nil {
	// 	log.Printf("Error listening and serving HTTP: %v\n", err)
	// }
	select {}
}
